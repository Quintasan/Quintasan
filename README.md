# Hello :wave:

Hello there, I'm Michał (ME-how, but feel free to call me Michael). I'm an backend engineer in [Threat Insights team](https://about.gitlab.com/handbook/engineering/development/sec/govern/threat-insights/) at GitLab. Majority of the time I work on [Vulnerability Management](https://about.gitlab.com/direction/govern/threat_insights/vulnerability_management/) but you can also find me working on database aspects of GitLab itself.

## Availablility :calendar_spiral:

I live in Wrocław, Poland :flag_pl:. This is usually UTC+1 or UTC+2 (depending on the season). I usually work between 10:00 UTC - 16:00 UTC. Feel free to schedule a meeting with me outside of those hours and we should be able to figure something out that works for both of us!

My workday usually looks like this:

1. Code review (1-2 hours)
1. Other TODOs (usually < 1 hour)
1. Work on issues/merge requests/attend meetings (rest of the time)
1. Check on code review requests for the next day

I'm in the process of trying to get my new apartment furnished so the above timeline might vary by quite a lot as I often find myself having to make multiple phone calls or commute to the apartment to explain things.

If my GitLab status is Busy or :red_circle: or a combination of both then it means I'm at capacity and cannot take on more new tasks/code review requests. However, do assign things to me if:

1. I already reviewed/commented on your issue/merge request and you want me to take another look
1. Your issue/merge request is directly releated to Threat Insights group
1. There's noone else left in the pool

## Communication style :speech_left:

My preferences can be roughly summed up as: mention on merge request/issue > Slack > sync call > email > phone call.

If you want my input on a technical problem then having answers to the following questions would be really helpful:

1. What are you trying to accomplish? (Describe the high-level goal of the problem you're trying to solve)
1. What have you tried so far? What were the results?
1. What are the assumptions you're working under?
1. Copypastable versions of logs/configuration files/code or whatever you're dealing with, if possible.

## Other communication preferences :bulb:

These are copied/adjusted from the [Diversity, Inclusion and Belonging team member profile](https://gitlab.com/gitlab-com/people-group/dib-diversity-inclusion-and-belonging/diversity-and-inclusion/-/blob/master/.gitlab/issue_templates/Team-Member-Profile.md) template.

- I prefer to communicate through written communications
- I can communicate well in large groups
- I can go off on a tangent, don't hesitate to stop me if that happens

## Working style 👨‍💻

- I tend to need time to process information before providing answers (otherwise my answer will most likely be: "It depends")
- I can work on no more than 4 issues/merge requests at once. Please let me know if something is high priority so I can effectively select things to work on at the moment.
